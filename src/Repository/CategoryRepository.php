<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }
    public function getCategoriesTree()
    {
        $categoriesList = $this->findAll();
        $firstLevelCategoriesList = array();
        $secondLevelCategoriesList = array();
        $thirdLevelCategoriesList = array();
        foreach ($categoriesList as $category) {
            switch ($category->getLevel()) {
                case "1":
                    $firstLevelCategoriesList[$category->getId()]= array(
                        'label'=>$category->getLabel(),
                        'hasSubCategories'=>true,
                        'subCategories'=>array(),
                    );
                    break;
                case "2":
                    $secondLevelCategoriesList[$category->getId()]= array(
                        'label'=>$category->getLabel(),
                        'hasSubCategories'=>true,
                        'subCategories'=>array(),
                        'parent'=>$category->getParent()->getId(),
                    );
                    break;
                case "3":
                    $thirdLevelCategoriesList[$category->getId()]= array(
                        'label'=>$category->getLabel(),
                        'hasSubCategories'=>false,
                        'parent'=>$category->getParent()->getId(),
                    );
                    break;
            }
        }
        foreach ($thirdLevelCategoriesList as $id=> $thirdLevelCategory) {
            $secondLevelCategoriesList[$thirdLevelCategory["parent"]]["subCategories"][$id]= $thirdLevelCategory;
        }

        foreach ($secondLevelCategoriesList as  $id=>$secondLevelCategory) {
            $firstLevelCategoriesList[$secondLevelCategory["parent"]]["subCategories"][ $id]= $secondLevelCategory;
        }
        return $firstLevelCategoriesList;
    }

    // /**
    //  * @return Category[] Returns an array of Category objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
