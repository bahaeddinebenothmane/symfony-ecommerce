<?php

namespace App\Service;

use App\Repository\CategoryRepository;

class CacheData{
    
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }
    public function getCategoriesTree()
    {
        return $this->categoryRepository->getCategoriesTree();
    }
}