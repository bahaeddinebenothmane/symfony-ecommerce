<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoryRepository;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(CategoryRepository $categoryRepository)
    {
        
        return $this->render('home/index.html.twig', [
            'chosenLanguage' => 'English',
            'languagesList' => array('English', 'Italian', 'Spanish', 'Japanese'),
            'currenciesList' => array('EUR Euro', 'GBP British Pound', 'JPY Japanese Yen'),
            'categoriesList'=>$categoryRepository->getCategoriesTree(),
            'categoriesFromRepo' => array(
                array(
                    'label' => 'firstLevelCategory1',
                    'hasSubCategories' => true,
                    'subCategories' => array(
                        array(
                            'label' => 'secondLevelCategory1',
                            'hasSubCategories' => true,
                            'subCategories' => array(
                                array(
                                    'label' => 'thirdLevelCategory1',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory2',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory3',
                                    'hasSubCategories' => false,
                                ),
                            ),
                        ),
                        array(
                            'label' => 'secondLevelCategory2',
                            'hasSubCategories' => true,
                            'subCategories' => array(
                                array(
                                    'label' => 'thirdLevelCategory1',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory2',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory3',
                                    'hasSubCategories' => false,
                                ),
                            ),
                        ),
                        array(
                            'label' => 'secondLevelCategory3',
                            'hasSubCategories' => true,
                            'subCategories' => array(
                                array(
                                    'label' => 'thirdLevelCategory1',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory2',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory3',
                                    'hasSubCategories' => false,
                                ),
                            ),
                        ),
                    ),
                ),
                array(
                    'label' => 'firstLevelCategory2',
                    'hasSubCategories' => true,
                    'subCategories' => array(
                        array(
                            'label' => 'secondLevelCategory1',
                            'hasSubCategories' => true,
                            'subCategories' => array(
                                array(
                                    'label' => 'thirdLevelCategory1',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory2',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory3',
                                    'hasSubCategories' => false,
                                ),
                            ),
                        ),
                        array(
                            'label' => 'secondLevelCategory2',
                            'hasSubCategories' => true,
                            'subCategories' => array(
                                array(
                                    'label' => 'thirdLevelCategory1',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory2',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory3',
                                    'hasSubCategories' => false,
                                ),
                            ),
                        ),
                        array(
                            'label' => 'secondLevelCategory3',
                            'hasSubCategories' => true,
                            'subCategories' => array(
                                array(
                                    'label' => 'thirdLevelCategory1',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory2',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory3',
                                    'hasSubCategories' => false,
                                ),
                            ),
                        ),
                    ),
                ),
                array(
                    'label' => 'firstLevelCategory3',
                    'hasSubCategories' => true,
                    'subCategories' => array(
                        array(
                            'label' => 'secondLevelCategory1',
                            'hasSubCategories' => true,
                            'subCategories' => array(
                                array(
                                    'label' => 'thirdLevelCategory1',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory2',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory3',
                                    'hasSubCategories' => false,
                                ),
                            ),
                        ),
                        array(
                            'label' => 'secondLevelCategory2',
                            'hasSubCategories' => true,
                            'subCategories' => array(
                                array(
                                    'label' => 'thirdLevelCategory1',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory2',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory3',
                                    'hasSubCategories' => false,
                                ),
                            ),
                        ),
                        array(
                            'label' => 'secondLevelCategory3',
                            'hasSubCategories' => true,
                            'subCategories' => array(
                                array(
                                    'label' => 'thirdLevelCategory1',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory2',
                                    'hasSubCategories' => false,
                                ),
                                array(
                                    'label' => 'thirdLevelCategory3',
                                    'hasSubCategories' => false,
                                ),
                            ),
                        ),
                    ),
                ),
            )
        ]);
    }
}
